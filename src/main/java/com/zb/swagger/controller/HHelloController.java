package com.zb.swagger.controller;

import com.zb.swagger.pojo.User;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HHelloController {

    @GetMapping(value = "/hello")
    public  String hello (){
        return "Hello";

    }
    @PostMapping(value = "/user")
    public User user(){
        return  new User();


    }

    @ApiOperation("hello控制类")
    @GetMapping(value = "/hello2")
    public  String hello2 (@ApiParam("用户名") String username){
        return "Hello"+username;

    }


}
