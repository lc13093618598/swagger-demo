package com.zb.swagger.controller.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2//开启swagger2
public class SwaggerConfig {
    //配置了swagger 的Docket 的beban 实例
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo())
                //这个是分组
                .groupName("雷超")
                .select()
                //RequestHandlerSelectors配置接口方式
                //basePackage：指定要扫描的包.basePackage("com.zb.swagger.controller")
                //.any() :扫描所有包
                //none() :不扫描
                //withMethodAnnotation 扫描类上注解，参数是一个注解的反射对象
                //withclassAnnotation :扫描方法上的注解
                .apis(RequestHandlerSelectors.basePackage("com.zb.swagger.controller"))
                //paths（）.过滤什么什么路径
              //  .paths(PathSelectors.ant("/zb/**"))
                .build();
    }
    //配置Swagger 信息==apiInfo
    private ApiInfo apiInfo() {
        //作者信息
      Contact contact=  new Contact("雷超", "http://www.xiaoxiaochao.club:82/image/", "3070414283@qq.com");
        return new ApiInfo(
                "雷超的SwaggerApi文档",
                "这个作者",
                "1.0",
                "http://www.xiaoxiaochao.club:82/image/",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());
    }
}
