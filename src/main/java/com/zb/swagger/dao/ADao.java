package com.zb.swagger.dao;

import com.zb.swagger.pojo.A;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public  interface  ADao {
      @Select(value = "select * from a")
      public   List<A> all();
}
