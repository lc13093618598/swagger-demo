package com.zb.swagger.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("这个是A")
public class A {
    @ApiModelProperty("这是一个id")
    public int id;
    @ApiModelProperty("这是一个name")
   public String name;
}
