package com.zb.swagger.service.impl;

import com.zb.swagger.dao.ADao;
import com.zb.swagger.pojo.A;
import com.zb.swagger.service.Aservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AserviceImpl implements Aservice {
    @Autowired
    public ADao aDao;

    @Override
    public List<A> all() {
        return aDao.all();
    }
}
